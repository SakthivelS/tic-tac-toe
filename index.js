let xClas = "x";
let circleClass = "circle";
const cells = document.querySelectorAll("[data-cell]");
const board = document.getElementById("board");
const winningMessage = document.getElementById("winning-message");
const winningMessageText = document.querySelector("[data-winning-message]");
const restartButtton = document.getElementById("restartButton");
const clickAudio = new Audio("./click.mp3");
const winAudio = new Audio("./win.mp3");
let circleTurn;
let winningCombinations = [
    [0, 1, 2],
    [3, 4, 5],
    [6, 7, 8],
    [0, 3, 6],
    [1, 4, 7],
    [2, 5, 8],
    [0, 4, 8],
    [2, 4, 6],
];

board.classList.add(xClas);

cells.forEach((cell) => {
    cell.addEventListener("click", hancleClick, { once: true });
});

restartButtton.addEventListener("click", () => {
    window.location = "/";
});

function hancleClick(e) {
    const cell = e.target;
    const currentClass = circleTurn ? circleClass : xClas;
    //placeMark
    cell.classList.add(currentClass);

    //check for win
    if (checkWin(currentClass)) {
        endGame(false);
    } else if (isDraw()) {
        endGame(true);
    } else {
        //switchTurn
        circleTurn = !circleTurn;
        board.classList.remove(currentClass);
        board.classList.add(circleTurn ? circleClass : xClas);
        clickAudio.play();
    }
}

function checkWin(currentClass) {
    return winningCombinations.some((combination) => {
        return combination.every((index) => {
            return cells[index].classList.contains(currentClass);
        });
    });
}

function endGame(draw) {
    if (draw) {
        winningMessageText.innerText = "Draw !!";
    } else {
        winningMessageText.innerText = `${circleTurn ? "O's" : "X's"} Won!`;
    }
    winningMessage.classList.add("show");
    winAudio.play();
}

function isDraw() {
    return [...cells].every((cell) => {
        return (
            cell.classList.contains(xClas) ||
            cell.classList.contains(circleClass)
        );
    });
}
